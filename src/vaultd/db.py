from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Session = sessionmaker()

Base = declarative_base()

def init(Config):
    '''
    Main procedure to initialize db connection of application
    Given a Config-class got due to apply ConfigParser module 
    '''
    dbdriver = Config.get('db', 'driver')
    user = Config.get('db', 'user')
    passwd = Config.get('db', 'pass')
    host = Config.get('db', 'host')
    db   = Config.get('db', 'db')
    dbconf = (("%s://%s:%s@%s/%s") % (dbdriver, user, passwd, host, db))

    __dbEngine = create_engine(dbconf, pool_size=0, echo=False)
    Session = sessionmaker()
    Session.configure(bind=__dbEngine)

    Base.metadata.bind = __dbEngine
    
    #Create base structure on server platform
    from purse import Account, Operation, Group
    try:
        Base.metadata.create_all()   
    except:
        raise Exception ('Failed initialize Databasee structure! \
maybe cannot connect to DB or database not created') 
        
    return __dbEngine
