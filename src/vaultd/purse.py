import datetime

from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy.types import Integer, SmallInteger, Float, String, DateTime
from sqlalchemy.orm import relationship

from db import Base

association_table = Table('group2account', Base.metadata,
    Column('id', Integer, primary_key=True),
    Column('account_id', Integer, ForeignKey('accounts.id')),
    Column('group_id', Integer, ForeignKey('groups.id'))
)
#ACCOUNT/GROUP ACTIVITY
ACTIVITY_ALL    = -1
ACTIVITY_ARCHIV = 0
ACTIVITY_ALIVE = 1

class Account(Base):
    '''
    Main class to provide Purse Account structure:
    base structure, initialize, represent and give actuality archive status 
    class methods 
    '''
    __tablename__ = 'accounts'
    
    id       = Column(Integer, primary_key=True)
    number   = Column(String(12), nullable = False)
    currency = Column(String(5), nullable = False)
    ctime    = Column(DateTime, nullable = False, default = datetime.datetime.now)
    balance  = Column(Float, default=0.0)    
    blFunds  = Column(Float, default=0.0)
    state    = Column(SmallInteger, default=ACTIVITY_ALIVE)

    groups = relationship("Group",
                          secondary=association_table,
                          single_parent=True,
                          cascade="all, delete",
                          backref="accounts",
                          passive_deletes=True,
                          )    
    
    def __init__(self, currency, number='', balance=0.0, blFunds=0.0):
        self.number = str(number)
        self.currency = currency
        self.balance = balance
        self.blFunds = blFunds
        self.state = ACTIVITY_ALIVE 

    def __repr__(self):
        return ("<Account('%s', '%s', '%s', '%s')>" 
                % (self.number, self.balance, self.blFunds, self.currency))
    
    def toDict(self):
        return dict(freeFunds = self.balance - self.blFunds,
                    total = self.balance,
                    number = self.number,
                    currency = self.currency,
                    state = self.state,
                    groups = [g.toDict() for g in self.groups])
    
    def isArchive(self):
        return not (self.state == ACTIVITY_ALIVE)


#BALANCE TYPE
TYPE_BALANCE = 0
TYPE_BLOCK   = 1

#OPERATION STATUS
STATUS_DONE    = 0
STATUS_FAILED  = 1

#FAIL CODES
FC_OK                       = 0
FC_NOT_ENOUGH_FREE_FUNDS    = 1
FC_NOT_ENOUGH_BLOCKED_FUNDS = 2
FC_ARCHIVED_PURSE           = 3
FC_ILLEGAL_AMOUNT           = 4

class Operation(Base):
    '''
    Main class to provide Purse Account Operations structure:
    base structure, initialize and represent class methods
    '''
    __tablename__ = 'operations'
    
    id        = Column(Integer, primary_key=True)
    type      = Column(SmallInteger, nullable = False)
    accountId = Column("account_id", Integer, ForeignKey('accounts.id'), 
                       nullable = False)
    status    = Column(SmallInteger, default = STATUS_DONE)
    failcode  = Column("fail_code", String, default = FC_OK)
    ctime  = Column(DateTime, nullable = False, default = datetime.datetime.now)
    amount    = Column(Float, default=0.0)
    comment   =  Column(String, default = '')
    
    accounts  = relationship("Account")
  
    def __init__(self, type, account_id, amount, status=STATUS_DONE, 
                 failcode=FC_OK, comment='', id=None):
        self.type = type
        self.status = status
        self.accountId = account_id
        self.failcode = failcode
        self.amount = amount
        self.comment = comment
        self.id = id
    
    def __repr__(self):
        return ("<Operation('%s', '%s', '%s', '%s', '%s', '%s', '%s')>" 
                % (self.id, self.type, self.status, self.failcode, self.ctime,  
                   self.amount, self.comment))
    
    def toDict(self):
        return dict(id = self.id,
                    type = self.type,
                    status = self.status,
                    accountId = self.accountId,
                    failCode = self.failcode,
                    time = self.ctime, 
                    amount = self.amount,
                    comment = self.comment)


class Group(Base):
    '''
    Main class to provide Purse Account Group structure:
    base structure, initialize and represent class methods
    '''
    __tablename__ = 'groups'
    
    id    = Column(Integer, primary_key=True)
    name  = Column(String(64), nullable=False)
    state = Column(SmallInteger, default=ACTIVITY_ALIVE)

    def __init__(self, name, id=None):
        self.name = name
        self.id = id
        self.state = ACTIVITY_ALIVE 
    
    def __repr__(self):
        return ("<group('%s', '%s'>" 
                % (self.id, self.name))
    
    def toDict(self):
        return dict(id = self.id,
                    name = self.name)

    def isArchive(self):
        return not(self.state == ACTIVITY_ALIVE)
