import sys
import argparse
import ConfigParser
import xmlrpclib

from twisted.python import log

from vaultd import db

class Verificator(object):
    def __init__(self):
        configfile=self.parseCommandLineOptions()
        
        Config = ConfigParser.ConfigParser()
        Config.read(configfile)
        
        logdest=Config.get('log', 'output')
        if logdest=='stdout':
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open(logdest,'w'))
            
        __EngineOwn = db.init(Config)

        hermes_conn =("https://%s:%s@%s:%s" % 
                                       (Config.get('hermes', 'user'), 
                                        Config.get('hermes', 'pass'), 
                                        Config.get('hermes', 'host'), 
                                        Config.get('hermes', 'port')))
        if Config.get('hermes', 'service'):
            hermes_conn = hermes_conn + "/" + Config.get('hermes', 'service')
        self.hermes = xmlrpclib.Server(hermes_conn)

        billing_conn =("https://%s:%s@%s:%s" % 
                                       (Config.get('billing', 'user'), 
                                        Config.get('billing', 'pass'), 
                                        Config.get('billing', 'host'), 
                                        Config.get('billing', 'port')))
        if Config.get('billing', 'service'):
            billing_conn = billing_conn + "/" + Config.get('billing', 'service')
        self.billing = xmlrpclib.Server(billing_conn)

        self.verifyOwn(__EngineOwn)
        self.verifyWithRemote(__EngineOwn, Config.get('billing', 'vaultident'))
        
    def verifyOwn(self, engine):
        query_string=("select acc.number as purse, (acc.balance-sum(oper.amount)) as balanceDiff, (acc.\"blFunds\"-sum(oper2.amount)) as blockFundsDiff from accounts acc left join operations oper  on (oper.status=0) and (oper.\"type\"=0) and (oper.account_id=acc.id) left join operations oper2 on (oper.status=0) and (oper.\"type\"=1) and (oper.account_id=acc.id) group by acc.number, acc.balance, acc.\"blFunds\";")
        session = engine.connect()
        rows = session.execute(query_string)
        for e in rows:
            if e[1] or e[2]:
                message = "Money-vault auditor. Attention! Look at purse(purse number, balance differ, block funds differ): " + str(e)
                self.signal(message)
    
    def verifyWithRemote(self, engine, selfIdent):
        session = engine.connect()
        acc_rows = session.execute("select acc.id, acc.number from accounts acc;")
        for e in acc_rows:
            criteria = {
                       'filter': {
                                  'accounts':['account://%s/%s' % (selfIdent,e[1])]
                                  },
                       'needcount': False
                       }
            rtransactions = self.billing.getTransactionList(criteria)['items']
            op_count = 0
            for t in rtransactions:
                op_count += len(t['orders'])

            op_counts_rows = session.execute("select count(*) from operations where account_id=%s;" % (e[0]))
            for o in op_counts_rows:
                if (o[0] <> op_count):
                    self.signal('There are not equal count of operations own with balance\'s purse number %s. Own operations - %s, remote - %s' % (e[1],o[0], op_count))

            for t in rtransactions:
                for o in t['orders']:
                    op_rows = session.execute("select * from operations where id=%s;" % (o['remoteOrderId']))
                    own_opers = 0
                    for o in op_rows:
                        own_opers += 1
                    if own_opers <> 1:
                        self.signal('No own operations for remote operation: ' + str(o))

            operations_rows = session.execute("select * from operations where account_id=%s;" % (e[0]))
            for operation in operations_rows:
                isPresent = False
                for t in rtransactions:
                    for order in t['orders']:
                        if order['remoteOrderId'] == operation[0]:
                            isPresent = True 
                if isPresent == False:
                    self.signal('No remote operations for own operation: ' + str(operation))
                    
            
    def parseCommandLineOptions(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', default = 'tests.cfg')
        args = parser.parse_args()
        return args.config

    def signal(self, message):
        log.msg(message)
#        self.hermes.notify(message)
        pass



def main():
    Verificator()
    
if __name__ == "__main__":
    main()