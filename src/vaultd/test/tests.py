#!/usr/bin/python
import ConfigParser
from twisted.trial import unittest 
from twisted.python.failure import Failure

import vaultd.test.fixtures as fix
from vaultd.application import BusinessLogic
'''
Preview.
Its unittests for money-vault class-service
'''

'''
Testing Money vault business logic
'''
class BusLogicTest(unittest.TestCase):

    def getFailed(self, f, methodname):
        try:
            f.trap([Failure, Exception])
        except Exception, ex:   
            self.assertFalse('Test %s FAILED' % (methodname))
            raise ex
        except Failure, fa:
            self.assertFalse('Test %s FAILED' % (methodname))
            raise fa

    def getDone(self, x, awaiting, msg):
        self.assertEquals(str(x), str(awaiting), msg)

    def getDoneFC(self, x, awaiting, msg):
        self.assertEquals(str(x['failcode']), str(awaiting), msg)

    def getException(self, f, awaiting, msg):
        try:
            f.trap([Failure, Exception])
        except Exception, ex:   
            self.assertEquals(str(ex), str(awaiting), msg)
        except Failure, fa:
            self.assertEquals(str(fa.value), str(awaiting), msg)


    def setUp(self):
        self.Config = ConfigParser.ConfigParser()
        self.Config.read('tests.cfg') 
        self.access = BusinessLogic(self.Config)
        fix.all_to_default()

    def tearDown(self):
        pass

class TestCreatePurse(BusLogicTest):
    '''
    Texting methods for create purse account
    '''
    def testCreateNotExistPurseWithAllCorrectParameters(self):
        '''
        Normal create p.account
        @params: currency, number, group
        '''
        purse = {'currency': 'RUB', 'number': 7, 'groups': ['first']}
        msg = 'createPurse: Purse must be non-existing!'

        def getDone(x, awaiting, msg):
            self.assertEquals(str(x), str(purse['number']), msg)

            from vaultd.purse import Account
            session = fix.Session()
            msg = 'createPurse: not awaiting groups by created purse'
            self.assertEqual(len(session.query(Account).
                                 filter(Account.number==str(purse['number'])).
                                 first().toDict()['groups']), 1, msg)
            session.close()
        d = self.access.createPurse(purse)
        d.addCallback(getDone, purse['number'], msg)
        d.addErrback(self.getFailed, 'testCreateNotExistPurseWithAllCorrectParameters')

    def testCreateG2AtoNotExistPurseWithExistNotActiveGroup(self):
        '''
        Normal create p.account
        @params: currency, number, active groups
        '''
        awaiting = 'createPurse: At least one group in list not active!'
        purse = {'currency': 'RUB', 'number': 7, 'groups': ['other']}
        msg = 'createPurse: create purse but at least one group not active!'

        d = self.access.createPurse(purse)
        d.addCallback(self.getFailed, 'testCreateG2AtoNotExistPurseWithExistNotActiveGroup')
        d.addErrback(self.getException, awaiting, msg)

    def testCreateNotExistPurseWithNonExistGroup(self):
        '''
        Normal create p.account
        @params: currency, number, group
        '''
        name = 'fifth'
        purse = {'currency': 'RUB', 'number': 7, 'groups': [name]}
        awaiting = 'createPurse: Group %s not exist!' % name
        msg = 'createPurse: created purse but getting group not exist!'

        d = self.access.createPurse(purse)
        d.addCallback(self.getFailed, 'testCreateNotExistPurseWithNonExistGroup')
        d.addErrback(self.getException, awaiting, msg)

    def testCreateNotExistPurseWithoutGroup(self):
        '''
        Normal create p.account without group2account
        @params: currency, number
        '''
        purse = {'currency': 'RUB', 'number': 7}
        msg  = 'createPurse: created group2Account but create() was without getting group!'
        awaiting = str(purse['number'])

        d = self.access.createPurse(purse)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testCreateNotExistPurseWithoutGroup')

    def testCreatePurseWithoutNumber(self):
        '''
        Normal create p.account without number
        just not implemented in b.logic
        '''
        purse = {'currency': 'RUB', 'groups': ['first']}
        awaiting = 'createPurse: No purse number. Not Emplemented'
        msg = 'createPurse: Create method with generating purse number not implement yet'

        d = self.access.createPurse(purse)
        d.addCallback(self.getFailed, 'testCreatePurseWithoutNumber')
        d.addErrback(self.getException, awaiting, msg)

    def testCreateNotExistPurseWithoutGroupsOrKeyError(self):
        '''
        Normal create p.account without number
        just not implemented in b.logic
        '''
        purse = {'currency': 'RUB', 'number': 7}
        awaiting = str(purse['number'])
        msg = 'createPurse: Created purse without array purse groups, at least []'

        d = self.access.createPurse(purse)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testCreateNotExistPurseWithoutGroupsOrKeyError')

    def testCreateExistPurse(self):
        '''
        Try to create exist p.account
        '''
        purse = {'currency': 'RUB', 'number': 4, 'groups': ['first']}
        awaiting = 'createPurse: Account already exist!'
        msg = 'createPurse: created purse with exist number!'

        d = self.access.createPurse(purse)
        d.addCallback(self.getFailed, 'testCreateExistPurse')
        d.addErrback(self.getException, awaiting, msg)


class TestClosePurse(BusLogicTest):
    '''
    Texting methods for close purse account
    '''
    def testCloseExistPurse(self):
        '''
        Try to close exist p.account
        '''
        awaiting = True
        msg = 'closePurse: can''t close exist non-archived account!'

        d = self.access.closePurse(4)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testCloseExistPurse')

    def testCloseNotExistPurse(self):
        '''
        Try to close not exist p.account
        '''
        awaiting = 'Try to close not exist Purse!'
        msg = 'closePurse: close not exist account!'

        d = self.access.closePurse(7)
        d.addCallback(self.getFailed, 'testCloseNotExistPurse')
        d.addErrback(self.getException, awaiting, msg)


class TestDeposit(BusLogicTest):
    '''
    Texting methods for deposit funds to purse account
    '''
    def testDepositNotExistPurse(self):
        '''
        Try to deposit funds to not exist p.account
        '''
        awaiting = 'Try deposit funds to not exist Purse Account'
        msg = 'deposit: done deposit to not exist account!'

        d = self.access.deposit(7, 3000)
        d.addCallback(self.getFailed, 'testDepositNotExistPurse')
        d.addErrback(self.getException, awaiting, msg)

    def testDepositExistPurse(self):
        '''
        Try to deposit funds to exist non-archived  p.account
        '''
        awaiting = '0'
        msg = 'deposit: failed deposit to exist non-archived account!'

        d = self.access.deposit(4, 3000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testDepositExistPurse')

    def testDepositExistArchivedPurse(self):
        '''
        Try to deposit funds to exist archived  p.account
        '''
        awaiting = '3'
        msg = 'deposit: done deposit to exist archived account!'

        d = self.access.deposit(5, 3000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testDepositExistArchivedPurse')

class TestWithdraw(BusLogicTest):
    '''
    Texting methods for withdraw funds to purse account
    '''
    def testWithdrawNotExistPurse(self):
        '''
        Try to deposit funds to not exist p.account
        '''
        awaiting = 'Try withdraw funds from not exist Purse Account'
        msg = 'withdraw: done withdraw to not exist account!'

        d = self.access.withdraw(7, 300)
        d.addCallback(self.getFailed, 'testWithdrawNotExistPurse')
        d.addErrback(self.getException, awaiting, msg)

    def testWithdrawExistPurse(self):
        '''
        Try to withdraw funds to exist non-archived  p.account.
        Blocked funds enough
        '''
        awaiting = '0'
        msg = 'withdraw: failed withdraw funds to exist \
non-archived account with enough free funds!'

        d = self.access.withdraw(4, 300)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testWithdrawExistPurse')

    def testWithdrawArchivedPurse(self):
        '''
        Try to withdraw funds to archived  p.account.
        '''
        awaiting = '3'
        msg = 'withdraw: done withdraw funds to archived account!'

        d = self.access.withdraw(5, 300)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testWithdrawArchivedPurse')

    def testWithdrawExistPurseNoFunds(self):
        '''
        Try to withdraw funds to exist non-archived  p.account.
        Blocked funds not enough
        '''
        awaiting = '2'
        msg = 'withdraw: done withdraw funds when not enough free funds!'

        d = self.access.withdraw(6, 300)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testWithdrawExistPurseNoFunds')


class TestBlockFunds(BusLogicTest):
    '''
    Texting methods for block free funds on purse account
    '''
    def testBlockFundsNotExistPurse(self):
        '''
        Try to block funds on not exist p.account
        '''
        awaiting = 'Try block funds on not exist Purse Account'
        msg = 'blockFunds: done block funds to not exist account!'


        d = self.access.blockFunds(7, 3000)
        d.addCallback(self.getFailed, 'testBlockFundsNotExistPurse')
        d.addErrback(self.getException, awaiting, msg)

    def testBlockFundsExistPurse(self):
        '''
        Try to block funds on exist non-archived p.account
        Free funds enough
        '''
        awaiting = '0'
        msg = 'blockFunds: failed block funds to exist \
non-archived account with enough free funds!'

        d = self.access.blockFunds(6, 3000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testBlockFundsExistPurse')

    def testBlockFundsArchivedPurse(self):
        '''
        Try to block funds on archived p.account
        '''
        awaiting = '3'
        msg = 'blockFunds: done block funds to exist archived account!'

        d = self.access.blockFunds(5, 3000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testBlockFundsArchivedPurse')

    def testBlockFundsExistPurseNoFunds(self):
        '''
        Try to block funds on exist non-archived p.account
        Free funds not enough
        '''
        awaiting = '1'
        msg = 'blockFunds: done block funds to exist \
non-archived account with not enough free funds!'

        d = self.access.blockFunds(4, 3000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testBlockFundsExistPurseNoFunds')


class TestReleaseFunds(BusLogicTest):
    '''
    Texting methods for release blocked funds on purse account
    '''
    def testReleaseFundsNotExistPurse(self):
        '''
        Try to release funds on not exist p.account
        '''
        awaiting = 'Try release funds on not exist Purse Account'
        msg = 'releaseFunds: done release funds to not exist account!'

        d = self.access.releaseFunds(7, 3000)
        d.addCallback(self.getFailed, 'testReleaseFundsNotExistPurse')
        d.addErrback(self.getException, awaiting, msg)

    def testReleaseFundsExistPurse(self):
        '''
        Try to Release funds on exist non-archived p.account
        Blocked funds enough
        '''
        awaiting = '0'
        msg = 'releaseFunds: failed release funds to exist \
non-archived account with enough blocked funds!'

        d = self.access.releaseFunds(4, 1000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testReleaseFundsExistPurse')

    def testReleaseFundsArchivedPurse(self):
        '''
        Try to release funds on archived p.account
        '''
        awaiting = '3'
        msg = 'releaseFunds: done release funds to exist archived account!'

        d = self.access.releaseFunds(5, 1000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testReleaseFundsArchivedPurse')

    def testReleaseFundsExistPurseNoFunds(self):
        '''
        Try to Release funds on exist non-archived p.account
        Blocked funds not enough
        '''
        awaiting = '2'
        msg = 'releaseFunds: done release funds to exist \
non-archived account with not enough blocked funds!'

        d = self.access.releaseFunds(6, 1000)
        d.addCallback(self.getDoneFC, awaiting, msg)
        d.addErrback(self.getFailed, 'testReleaseFundsExistPurseNoFunds')


class TestGroup(BusLogicTest):
    '''
    Texting group methods
    '''
    def __getDoneCount(self, x, awaiting, msg):
        self.assertEquals(str(x.count()), awaiting, msg)


    def testCreateNotExistGroup(self):
        '''
        Try to create not exist group
        '''
        name = 'fifth'
        awaiting = 'fifth'
        msg = 'createGroup: created just exist group!'

        d = self.access.createGroup(name)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testCreateNotExistGroup')

    def testCreateExistGroup(self):
        '''
        Try to create exist group
        '''
        name = 'first'
        awaiting = 'createGroup: Group already exist!'
        msg = 'createGroup: create exist group!'

        d = self.access.createGroup(name)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testCreateExistGroup')

    def testGetGroups(self):
        '''
        Try to get group list
        '''
        awaiting = '3'
        msg = 'getGroup: all groups less then await!'

        d = self.access.getGroups(-1)
        d.addCallback(self.__getDoneCount, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetGroups')

    def testGetGroups2(self):
        awaiting = '2'
        msg = 'getGroup: alive groups less then await!'

        d = self.access.getGroups()
        d.addCallback(self.__getDoneCount, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetGroups2')

    def testGetGroups3(self):
        awaiting = '1'
        msg = 'getGroup: archiv groups less then await!'

        d = self.access.getGroups(0)
        d.addCallback(self.__getDoneCount, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetGroups3')

    def testChangeActivityForExistGroup(self):
        awaiting = '0'
        msg = 'changeGroupActivity: failed change activity for exist group!'

        d = self.access.changeGroupActivity('first', 0)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testChangeActivityForExistGroup')

    def testChangeActivityForNotExistGroup(self):
        awaiting = 'changeGroupActivity: Not exist group!'
        msg = 'changeGroupActivity: must not change activity for exist group!'

        d = self.access.changeGroupActivity('fifth', 0)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testChangeActivityForNotExistGroup')


class TestGetPurseInfo(BusLogicTest):
    '''
    Testing get purse info methods
    '''
    def testGetAccountInfoExistPurse(self):
        awaiting = {'number': '5',
                    'currency': 'RUB',
                    'ctime': '2011-08-15 12:00:00',
                    'state': 0,
                    'freeFunds': 1000.0,
                    'balance': 2000.0,
                    'groups': [{'name': 'other', 'id': 3},
                               {'name': 'first', 'id': 1}]}

        msg = 'getPurseInfo: not awaiting purse account info'

        def getDone(x, awaiting, msg):
            self.assertEqual(x['balance'], awaiting['balance'], msg)
            self.assertEqual(str(x['ctime']), awaiting['ctime'], msg)
            self.assertEqual(len(x['groups']), len(awaiting['groups']), msg)
        d = self.access.getPurseInfo(awaiting['number'])
        d.addCallback(getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetAccountInfoExistPurse')

    def testGetAccountInfoNotExistPurse(self):
        number = 7
        awaiting = 'getPurseInfo: Purse not exist'
        msg = 'getPurseInfo: must get exception about no purse'

        d = self.access.getPurseInfo(number)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetAccountInfoNotExistPurse')

class TestGetPurseList(BusLogicTest):
    '''
    Testing get purse list methods
    '''
    def testGetPurseListWithoutCriteria(self):
        awaiting = '3'
        msg = 'getPurseList: count of all purses differ then await'

        d = self.access.getPurseList()
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithoutCriteria')

    def testGetPurseListWithCriteriaByCurrency(self):
        criteria = {'currency': 'RUB'}
        awaiting = '2'
        msg = 'getPurseList: count of purses by currency differ then await'

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByCurrency')

    def testGetPurseListWithCriteriaAccountsList(self):
        criteria = {'numbers': ['4', '6']}
        awaiting = '2'
        msg = 'getPurseList: count of purses by currency differ then await'

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaAccountsList')

    def testGetPurseListWithCriteriaByExistGroup(self):
        criteria = {'groups': ['first']}
        awaiting = '2'
        msg = 'getPurseList: count of purses by exist group differ then await /test 1 of 2/'

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByExistGroup')

    def testGetPurseListWithCriteriaByExistGroup2(self):
        criteria = {'groups': ['other']}
        awaiting = '1'
        msg = 'getPurseList: count of purses by exist group differ then await /test 2 of 2/'

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByExistGroup2')

    def testGetPurseListWithCriteriaByNotExistGroup(self):
        criteria = {'groups': ['fifth']}
        awaiting = 'Group %s not exist!' % criteria['groups'][0]
        msg = 'getPurseList: count of purses by not exist group differ then await'

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByNotExistGroup')

    def testGetPurseListWithCriteriaByTimeRange1(self):
        criteria = {'startTime': '2011-08-15'}
        awaiting = '2'
        msg = ('getPurseList: count of purses by time range (only start time) \
differ then await /test 1 of 3/')

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByTimeRange1')

    def testGetPurseListWithCriteriaByTimeRange2(self):
        criteria = {'endTime': '2011-09-01'}
        awaiting = '2'
        msg = ('getPurseList: count of purses by time range (only end time) \
 differ then await /test 2 of 3/')

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByTimeRange2')

    def testGetPurseListWithCriteriaByTimeRange3(self):
        criteria = {'startTime': '2011-09-01', 'endTime': '2011-08-01'}
        awaiting = '0'
        msg = ('getPurseList: count of purses by time incorrect range  \
differ then await /test 3 of 3/')

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaByTimeRange3')

    def testGetPurseListWithCriteriaOffset(self):
        criteria = {'offset': [0,2]}
        awaiting = '2'
        msg = ('getPurseList: count of purses by offset from all purses  \
differ then await')

        d = self.access.getPurseList(criteria)
        d.addCallback(self.getDone, awaiting, msg)
        d.addErrback(self.getFailed, 'testGetPurseListWithCriteriaOffset')
