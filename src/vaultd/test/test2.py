#!/usr/bin/python
import sys
import psycopg2
import random

from twisted.python import log
from twisted.web.xmlrpc import Proxy

from twisted.python.failure import Failure
from twisted.internet import reactor

'''
Preview.
Its test send totalOperations-count request to the remote service and count 
result balance of account/accounts for done requests. Request sends at one time 
in work and reactor will not stop later  
'''


#Common options
totalOperations = 100
#for accounts
currencies = ['RUB', 'EUR', 'USD']
accountsnumber = ['1']

'''
API dictionary represent structure of available api-functions with possible 
test sets of parameters
'''
appAPI = {
          'createPurse': 'provision',
          'closePurse': 'number',
          'deposit': 'number, amount',
          'withdraw': 'number, amount',
          'blockFunds': 'number, amount',
          'releaseFunds': 'number, amount',
          'getPurseInfo': 'number',
          'getPurseList': 'criteria',
          }
testParams = []

class aTest(object):
    def __init__(self):
        '''
        Logs init
        '''
        logdest="stdout"
        if logdest=='stdout':
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open(logdest,'w'))
        log.msg('Log init...done.')

        #Connection init
        host = "localhost"
        port  = 8001
        serv = 'RPC'
        servicedsn = "https://billing:billing@%s:%s/%s" % (host, port, serv)
        self.service = Proxy(servicedsn)
        log.msg('Connection init...done.')

        #clear db
        db = 'money-vault'
        psqldsn = "host=%s user=%s password=%s dbname=%s" % (host, 'postgres', 'postgres', db)
        con = psycopg2.connect(psqldsn)
        cur = con.cursor()
        cur.execute('delete from operations')
        cur.execute('delete from group2account')
        cur.execute('delete from groups')
        cur.execute('delete from accounts')
        con.commit()
        con.close()
        log.msg('Clear database...done.')

        #init database with definite values (random currency)
        self.accounts = {}
        def createDone(c):
            log.msg('Purse %s created' % (c))
            con = psycopg2.connect(psqldsn)
            cur = con.cursor()
            cur.execute('select id from accounts where number=\'%s\';' % (c))
            self.accounts[c]['id'] = cur.fetchone()[0]
            con.commit()
            con.close()
        def createFailed(f):
            try:
                f.trap([Failure, Exception])
            except Exception, ex:
                log.msg('Create purse. exception!!! %s' % (str(ex)))
                self.unknownErrors += 1
            except Failure, fa:
                try:
                    if fa.value.faultCode==100:
                        self.counter += 1
                        log.msg('Create purse. Application failure: %s' % (str(fa)))
                    else:
                        self.unknownErrors += 1
                        log.msg('Create purse. Unknown failure code: %s' % (fa.value.faultCode))
                except:
                    self.unknownErrors += 1
                    log.msg('Unknown failure: %s' % str(fa))
        for acc in accountsnumber:
            currency = random.sample(currencies, 1)[0]
            provision = {'currency': currency, 'number': acc, 'groups': []}
            self.accounts[provision['number']] = {
                                                  'number': provision['number'],
                                                  'currency': provision['currency'],
                                                  'total' : 0,
                                                  'blFunds': 0,
                                                  }
            provision = (provision, )
            method = 'createPurse'
            params = provision
            log.msg('created request: %s%s' % (method, params))
            self.service.callRemote(method, *params).addCallbacks(createDone, createFailed)
        log.msg('Creating purses tasks prepare...done')

        #countered variables
        self.requestNotReturnedYet = totalOperations
        self.requestsDone = 0
        self.requestsFailed = 0
        self.requestException = 0
        self.unknownErrors = 0
        
        #init test parameters. its prepare only for balance operations(with money)
        for i in range(0, totalOperations):
            number = random.sample(accountsnumber, 1)[0]
            currency = random.sample(currencies, 1)[0]
            amount = random.randrange(100, 100000, 100)

            nummoney = (number, amount)
            number = (number,)
            
            testParams.append(nummoney)
        random.shuffle(testParams)
        log.msg('Testing parameters prepare...done')
    
    def getAccNumber(self, id):
        for acc in self.accounts.keys():
            if str(self.accounts[acc]['id']) == str(id):
                return self.accounts[acc]['number']    
                
    def syziph(self):
        '''
        create requests
        '''
        def syziphDone(x):
            log.msg('deposit/withdraw operation: %s' % (str(x)))
            self.requestsDone += 1
            if str(x['status']) == '0':
                if str(x['type']) == '0':
                    #deposit+withdraw
                    self.accounts[self.getAccNumber(x['account_id'])]['total'] += x['amount']
                    if x['amount'] < 0:
                        self.accounts[self.getAccNumber(x['account_id'])]['blFunds'] += x['amount']
                else:
                    #block+release funds
                    self.accounts[self.getAccNumber(x['account_id'])]['blFunds'] -= x['amount']
        def syziphFailed(f):
            try:
                f.trap([Failure, Exception])
            except Exception, ex:
                log.msg('exception!!! %s' % (str(ex)))
                self.requestException += 1
            except Failure, fa:
                try:
                    if fa.value.faultCode==100:
                        self.requestsFailed += 1
                        log.msg('Application failure: %s' % (str(fa)))
                    else:
                        self.unknownErrors += 1
                        log.msg('Unknown failure code: %s' % (fa.value.faultCode))
                except:
                    self.unknownErrors += 1
                    log.msg('Unknown failure: %s' % str(fa))
        methods = ['deposit', 'withdraw', 'blockFunds', 'releaseFunds']
        for params in testParams:
            key = random.randint(0, len(methods)-1)
            method = methods[key]
            log.msg('created request: %s%s' % (method, params))
            self.service.callRemote(method, *params).addCallbacks(syziphDone, syziphFailed)
        reactor.callLater(10, self.register)
    
    def register(self):
        def Done(d, number):
            self.accounts[number]['remote_total'] = d['balance']
            self.accounts[number]['remote_blFunds'] = d['balance'] - d['freeFunds']
            self.requestNotReturnedYet = (totalOperations - self.requestsDone
                                     - self.requestsFailed - self.unknownErrors)
        def Failed(f, number):
            try:
                f.trap([Failure, Exception])
            except Exception, ex:
                log.msg('getPurseInfo exception!!! %s' % (str(ex)))
        accs = self.accounts
        for number in accs.keys():
            params = (number, )
            d = self.service.callRemote('getPurseInfo', *params)
            d.addCallback(Done, number) 
            d.addErrback(Failed, number)
        log.msg('Results of work: \nlag: %s, balances%s' 
                % (self.requestNotReturnedYet, str(accs)))
        log.msg('Requests: done/failed/unknownerror %s/%s/%s' 
                % (self.requestsDone, self.requestsFailed,self.unknownErrors))
        reactor.callLater(10, self.register)
    
    def run(self):
        '''  
        Purse application run
        '''
        log.msg('Test Application runs...')
        reactor.callLater(10, self.syziph)
        reactor.run()        
        
        
def main():
    test = aTest()
    test.run() 

if __name__ == "__main__":
    main()