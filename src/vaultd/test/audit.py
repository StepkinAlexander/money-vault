import sys
import argparse
import ConfigParser

from twisted.python import log

from vaultd import db

#this script verificate purses balanse with own operations
class Audit(object):
    def __init__(self):
        configfile=self.parseCommandLineOptions()
        
        Config = ConfigParser.ConfigParser()
        Config.read(configfile)
        
        logdest=Config.get('log', 'output')
        if logdest=='stdout':
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open(logdest,'w'))
            
        __Engine = db.init(Config)

        self.verify(__Engine)
        
    def verify(self, engine):
        query_string=("select acc.number as purse, (acc.balance-sum(oper.amount)) as balanceDiff, (acc.\"blFunds\"-sum(oper2.amount)) as blockFundsDiff from accounts acc left join operations oper  on (oper.status=0) and (oper.\"type\"=0) and (oper.account_id=acc.id) left join operations oper2 on (oper.status=0) and (oper.\"type\"=1) and (oper.account_id=acc.id) group by acc.number, acc.balance, acc.\"blFunds\";")
        session = engine.connect()
        rows = session.execute(query_string)
        for e in rows:
            if e[1] or e[2]:
                message = "Money-vault auditor. Attention! Look at purse(purse number, balance differ, block funds differ): " + str(e)
                log.msg(message)
#                self.signal2hermes(Config, message)
                 
            
    def parseCommandLineOptions(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', default = 'testconf.cfg')
        args = parser.parse_args()
        return args.config
    
    def signal2hermes(self, Config, message):
        import xmlrpclib
        user = Config.get('hermes', 'user')
        password = Config.get('hermes', 'pass') 
        host = Config.get('hermes', 'host')
        port = Config.get('hermes', 'port')
        server = xmlrpclib.Server("https://%s:%s@%s:%s" % user, password, host, port)
        server.notify(message)


def main():
    Audit()
    
if __name__ == "__main__":
    main()