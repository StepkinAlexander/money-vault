import ConfigParser

import vaultd.db as db
from vaultd.db import Session

configfile='/home/sas/git/money-vault/src/vaultd/test/tests.cfg'

def get_config(configfile):
    config = ConfigParser.ConfigParser()
    config.read(configfile)
    return config 

from vaultd.purse import Account, Operation, Group
__dbEngine = db.init(get_config(configfile))
    
accounts = {'4': {'id': 1, 'number': '4', 'balance': 3000, 'blFunds': 1000, 
                  'currency': 'RUB', 'state': 1, 'ctime': '2011-08-05 12:05'},
            '5': {'id': 2, 'number': '5', 'balance': 2000, 'blFunds': 1000, 
                  'currency': 'RUB', 'state': 0, 'ctime': '2011-08-15 12:00'},
            '6': {'id': 3, 'number': '6', 'balance': 4000, 'blFunds': 0, 
                  'currency': 'EUR', 'state': 1, 'ctime': '2011-09-01 12:00'},
           }

balances = { '4': {'total': 3000.0, 'freeFunds': 2000.0},
             '5': {'total': 2000.0, 'freeFunds': 1000.0},
             '6': {'total': 4000.0, 'freeFunds': 4000.0}}

operations = {}

groups   = {'first' : {'id': 1, 'name': 'first',  'state': 1},
            'second': {'id': 2, 'name': 'second', 'state': 1},
            'other' : {'id': 3, 'name': 'other',  'state': 0}}

group2account = {1: ['first'],
                  2: ['first', 'other'],
                  3: []}

def all_to_default():
    '''
    Set database to default state for test processing 
    '''
    def clear_all():
        tables = [Operation, Group, Account]
        session = Session()
        for g in session.query(Group).all():
            session.delete(g)
            session.commit()
        for t in tables:
            session.query(t).delete()
        session.commit()
        session.close()
    clear_all()
    groups_to_default()
    accounts_to_default()
    operations_to_default()

def groups_to_default():
    '''
    load default sets to the table Groups
    now not need and its empty
    '''
    session = Session()
    for grp in groups.itervalues():
        res = (session.query(Group)
               .filter(Group.name==str(grp['name'])).first())
        if not res:
            group = Group(grp['name'])
            group.state = grp['state']
        session.add(group)
        session.commit()
        grp['id'] = group.id
    session.close()
    
def accounts_to_default():
    '''
    load default sets to the table Accounts
    '''
    session = Session()
    for acc in accounts.itervalues():
        res = (session.query(Account)
               .filter(Account.number==str(acc['number'])).first())
        if not res:
            account = Account(acc['currency'], acc['number'])
            account.balance = acc['balance']
            account.blFunds = acc['blFunds']
            account.state = acc['state']
            account.ctime = acc['ctime']
            for g in group2account[acc['id']]:
                account.groups.append(session.query(Group).
                                      filter(Group.id==groups[g]['id']).first())
        session.add(account)
    session.commit()
    session.close()

def operations_to_default():
    '''
    load default sets to the table Operations
    now not need and its empty
    '''
    pass
