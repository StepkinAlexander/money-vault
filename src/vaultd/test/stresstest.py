#!/usr/bin/python
import sys
import random

from twisted.python import log
from twisted.python.failure import Failure
from twisted.internet import reactor
from twisted.web.xmlrpc import Proxy

from turnipp import Checker
'''
Preview.
its test send some queries to remote service at timer seconds for defined 
cycles
'''

'''
API dictionary represent structure of available api-functions with possible 
test sets of parameters
'''
options = {
           #connection settings
           'dsn': 'https://billing:billing@localhost:8001/RPC',
           #logger output
           'output': '/tmp/test.log',
#           'output': 'stdout',
           #cycle parameters
           'timer': 1,
           'queriesInCycle': 12,
           'totalCycles': 120,
           #replication servers settings
#           'master': 'host=10.0.14.187 user=postgres',
#           'slave': 'host=10.0.14.190 user=postgres',
           }

appAPI = {#you can to exclude some methods from testing - comment it in this dict
          'createPurse': 'provision',
          'closePurse': 'number',
          'deposit': 'number, amount',
          'withdraw': 'number, amount',
          'blockFunds': 'number, amount',
          'releaseFunds': 'number, amount',
          'getPurseInfo': 'number',
          'getPurseList': 'criteria',
          }
testParams = {
              'provision': [],
              'number': [],
              'number, amount': [],
              'criteria': [({},),({'currency': 'RUB'},), ({'groups': ['first']},), 
                           ({'groups': ['fifth']},), ({'currency': 'USD'},)]
              }
currencies = ['RUB', 'EUR', 'USD']
groups = [[], ['first'], ['second'], ['fifth'], ['first', 'second']]

#One or Every full output results
OneResult = False

class StressTest(object):
    def __init__(self):
        log.msg('Application&log init')
        logdest = options['output']
        if logdest=='stdout':
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open(logdest,'w'))

        '''
        timer get counter to 0 and cycle+=1, when cycle will equal totalcycle 
        (except for totalcycle=0, unlimited runs) cycle will interrupt
        '''
        self.timer = int(options['timer'])
        self.queriesInCycle = int(options['queriesInCycle'])  
        self.totalCycles = int(options['totalCycles'])
        
        self.counter = 0      
        self.unknownErrors = 0
        self.totalCycleN = 0
        self.results = []

        self.appConn =(options['dsn'])
        self.conn = Proxy(self.appConn)
        
        try:
            self.master = options['master']
            self.slave = options['slave']
            self.checker = Checker(self.master, self.slave)
            self.dbsize = 0
        except:
            self.checker = None
        '''
        init test parameters. Unique for own API, 
        for other API init test parameters need modifying 
        '''
        for i in range(0, 1000):
            number = random.randint(1, 10000)
            currency = random.sample(currencies, 1)[0]
            amount = random.randrange(100, 100000, 100)
            group = random.sample(groups, 1)[0]
            
            provision = ({'currency': currency, 'number': number, 'groups': group},)
            nummoney = (number, amount)
            number = (number,)
            
            testParams['provision'].append(provision)
            testParams['number'].append(number)
            testParams['number, amount'].append(nummoney)
        random.shuffle(testParams['provision'])
        random.shuffle(testParams['number'])
        random.shuffle(testParams['number, amount'])
        log.msg('testing data was created')
       
    def syziph(self):
        '''
        create requests
        '''
        def syziphDone(x):
            log.msg(str(x))
            self.counter += 1
        def syziphFailed(f):
            try:
                f.trap([Failure, Exception])
            except Exception, ex:
                log.msg('exception!!! %s' % (str(ex)))
                self.unknownErrors += 1
            except Failure, fa:
                try:
                    if fa.value.faultCode==100:
                        self.counter += 1
                        log.msg('Application failure: %s' % (str(fa)))
                    else:
                        self.unknownErrors += 1
                        log.msg('Unknown failure code: %s' % (fa.value.faultCode))
                except:
                    self.unknownErrors += 1
                    log.msg('Unknown failure: %s' % str(fa))

        self.totalCycleN += 1
        self.register(self.totalCycleN)
        if self.totalCycleN >= self.totalCycles:
            if OneResult:
                log.msg('Cycle No %s. results: %s' % (totalCycleN, str(self.results)))
            reactor.stop()
            
        for i in range(self.queriesInCycle):
            key = random.randint(0, len(appAPI)-1)
            method = appAPI.keys()[key]
            params = random.sample(testParams[appAPI[method]], 1)[0]
            log.msg('created request: %s%s' % (method, params))
            self.conn.callRemote(method, *params).addCallbacks(syziphDone, syziphFailed)
        reactor.callLater(self.timer, self.syziph)
    
    def register(self, cycleN):
        regRequets = self.counter
        regErrors = self.unknownErrors
        self.counter -= regRequets
        self.unknownErrors -= regErrors
        if self.checker:
            old = self.dbsize
            lags = self.checker.check()
            self.dbsize = self.checker.size 
            self.results.append({
                                 'cycle': cycleN, 
                                 'requests done': regRequets, 
                                 'replic lags': lags,
                                 'db growing': (int(self.dbsize) - int(old)), 
                                 'requests failed': regErrors,
                                 })
            
        else:
            self.results.append({
                                 'cycle': cycleN, 
                                 'requests done': regRequets, 
#                                 'replic lags': 'No check replication', 
                                 'requests failed': regErrors,
                                 })
        if OneResult:
            log.msg('Cycle No %s DONE.' % (cycleN))
        else:
            log.msg('Cycle No %s. results: %s' % (cycleN, str(self.results)))
    
    def run(self):
        '''  
        Purse application run
        '''
        log.msg('Test Application runs...')
        reactor.callLater(self.timer, self.syziph)
        reactor.run()        
        
        
def main():
    test = StressTest()
    test.run() 

if __name__ == "__main__":
    main()