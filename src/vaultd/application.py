#!/usr/bin/python
import sys
import ConfigParser
import argparse
import datetime

from xmlrpclib import Fault 
from twisted.python import log
from twisted.internet import reactor, ssl, defer
from twisted.web import xmlrpc, server, resource

from twisted.internet import threads
from twisted.python.failure import Failure

from vaultd import db, purse
from vaultd.db import Session
from vaultd.purse import Account, Group, Operation, association_table 

VERSION = '2012-07-19rev.2'

class XmlRpcFacade(xmlrpc.XMLRPC):
    '''
    XmlRpc facade for billing application 
    '''
    def __init__(self, service):
        xmlrpc.XMLRPC.__init__(self)
        self._service = service 
        
    def auth(self, user, passwd):
        return self._service.auth(user, passwd)

    def render_POST(self, request):
        '''
        As we need to provide authorization, we add this override method
        @note: Overrides the xmlrpc.XMLRPC.render_POST. Sadly, we can't use
        super() as the xmlrpc.XMLRPC is the old style class :(
        @todo: use twisted.cred
        @param request:
        '''
        from twisted.web import http
        user = request.getUser()
        passwd = request.getPassword()
        
        if user=='' and passwd=='':
            request.setResponseCode(http.UNAUTHORIZED)
            return 'Authorization required!'

        if not self.auth(user, passwd):
            request.setResponseCode(http.UNAUTHORIZED)
            return 'Authorization failed!'
        
        log.msg('request initiator: %s' % request.user) 
        return xmlrpc.XMLRPC.render_POST(self, request)
    
    def _ebRender(self, failure):
        if isinstance(failure.value, Fault):
            return failure.value
        log.err(failure)
        failure_code = 100
        return Fault(failure_code, str(failure.value.message))
        
    def xmlrpc_createPurse(self, purseProvision):
        return self._service.createPurse(purseProvision)

    def xmlrpc_closePurse(self, number):
        return self._service.closePurse(number)
    
    def xmlrpc_deposit(self, number, amount):
        return self._service.deposit(number, amount)
    
    def xmlrpc_withdraw(self, number, amount):
        return self._service.withdraw(number, amount)
    
    def xmlrpc_blockFunds(self, number, amount):
        return self._service.blockFunds(number, amount)
    
    def xmlrpc_releaseFunds(self, number, amount):
        return self._service.releaseFunds(number, amount)

    def xmlrpc_createGroup(self, name):
        return self._service.createGroup(name)
    
    def xmlrpc_getGroups(self, activity=purse.ACTIVITY_ALIVE):
        return  self._service.getGroups(activity)
    
# this method is not present in API
    def xmlrpc_changeGroupActivity(self, name, activity):
        return self._service.changeGroupActivity(name, activity)
    
    def xmlrpc_getPurseInfo(self, number):
        return self._service.getPurseInfo(number)
    
    def xmlrpc_getPurseList(self, criteria):
        return self._service.getPurseList(criteria)


class LockProvider(object):
    locks = dict()

    def get_lock(self, id):
        self.locks[id] = self.locks.get(id, defer.DeferredLock())
        return self.locks[id].acquire()

    def release(self, id):
        return self.locks[id].release()

    def get_lock_keys(self):
        return self.locks.keys()

    def clean(self):
        keys_to_clean = []
        for k,v in self.locks.iteritems():
            if not v.locked:
                keys_to_clean.append(k)
        for k in keys_to_clean:
            del self.locks[k]
        return True

class BusinessLogic(object):
    '''
    Business logic of application realization
    '''  
    def __init__(self, config):
        self.Config = config
        self.Blocks = LockProvider()
        
    def __getattr__(self, name):
        def handler(*args):
            try:
                if name not in dir(self):
                    raise Exception('Has no method(' + name + ')!!!')
                temp = args
                session = Session()
                args = [session]
                args.extend(temp)
#                args = [session,].extend(args)
                return getattr(self, name)(*args)
            except Exception, ex:
                log.msg(str(ex))
                raise ex
        name = '_' + name
        return handler

    def __getDone(self, p):
        return self.__logData(p)

    def __getFailed(self, f, method):
        try:
            f.trap([Failure, Exception])
        except Exception, ex:
            log.msg('%s exception: %s' % (method, str(ex)))
            log.msg(str(ex))
            raise ex
        except Failure, fa:
            log.msg('%s failure: %s' % (method, str(fa)))
            log.msg(str(fa))
            raise fa

    def __fin(self, x, session, number=None):
        try:
            self.Blocks.release(number)
        finally:
            session.close()
            return x
    
    def __logData(self, data):
        '''
        write text to log and return it
        '''
        log.msg('Answer: %s ' % str(data))
        return data
        
    def auth(self, user, passwd):
        return (user, passwd) == (self.Config.get('auth', 'user'), 
                                  self.Config.get('auth', 'passwd'))
        
    def __getAccountByNumber(self, session, number):
        return session.query(Account).filter(Account.number==str(number)).first()
    
    def __getGroupByName(self, session, name, with_lock=True):
        return session.query(Group).filter(Group.name==str(name)).first()
    
    def _createPurse(self, session, purseProvision):
        '''
        Purse Account create
        @param: dict = {'currency: currency, ['EUR', 'USD', 'RUB']
                        ['number': number], not mandatory parameter in future. 
                                            not implement yet, now its mandatory
                        ['groups': group]
        @result: purse account number
        '''
        def xquery(session, purseProvision):
            def handle(x, session, purseProvision):
                def getNumber(purseProvision):
                    number = purseProvision.get('number', None)
                    if (str(number) == '') or (number is None):
                        raise Exception('createPurse: No purse number. Not Emplemented')
                    return number
                def getCurrency(purseProvision):
                    currency = purseProvision.get('currency', None)
                    if currency is None:
                        raise Exception('createPurse: No purse currency. Failed')
                    return currency
                def getGroups(purseProvision):
                    list = purseProvision.get('groups', [])
                    groups = []
                    for g in list:
                        gcurr = self.__getGroupByName(session, g)
                        if not gcurr:
                            raise Exception('createPurse: Group %s not exist!' % g)
                        state = gcurr.state
                        if state == purse.ACTIVITY_ALIVE:
                            groups.insert(0, gcurr.toDict())
                        else:
                            raise Exception('createPurse: At least one group in list not active!')
                    return groups
                currency = getCurrency(purseProvision)
                number   = getNumber(purseProvision)
                groups   = getGroups(purseProvision)

                p = self.__getAccountByNumber(session,number)
                if p:
                    raise Exception('createPurse: Account already exist!')
                query = Account(currency, number)
                if groups:
                    for g in groups:
                        query.groups.append(session.query(Group).
                                              filter(Group.id==g['id']).first())
                session.add(query)
                session.commit()
                return query.number 
            
            d = self.Blocks.get_lock(purseProvision.get('number', None))
            d.addCallback(handle, session, purseProvision)
            d.addErrback(self.__getFailed, 'createPurse')
            return d
        log.msg('request: createPurse(%s)' % (purseProvision))
        
        d = xquery(session, purseProvision)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'createPurse')
        d.addBoth(self.__fin, session, purseProvision.get('number', None))
        return d    

    def _closePurse(self, session, number):
        '''
        Switch Purse Account to archive state
        @param: number - purse account number
        @result: boolean as result of execution
        '''

        def xquery(session, number):
            def handle(x, session, number):
                p = self.__getAccountByNumber(session,number)
                if not p:
                    raise Exception('Try to close not exist Purse!')
                p.state = purse.ACTIVITY_ARCHIV
                session.commit()
                return True
            
            d = self.Blocks.get_lock(number)
            d.addCallback(handle, session, number)
            d.addErrback(self.__getFailed, 'closePurse')
            return d

        log.msg('request: closePurse(%s)' % (number))

        d = xquery(session, number)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'closePurse')
        d.addBoth(self.__fin, session, number)
        return d    
    
    def _deposit(self, session, number, amount):
        '''
        Deposit funds to Purse Account
        @params: number - purse account number, 
                 funds amount in account's currency
        @result: dict - result operation dict
                {id, type, accountId, status, failcode, ctime, amount, comment}
        '''
        def xquery(session, number, amount):
            def handle(x, session, amount):
                p = self.__getAccountByNumber(session,number)
                if not p:
                    msg = 'Try deposit funds to not exist Purse Account'
                    log.msg(msg)
                    raise Exception(msg)
                elif p.isArchive():
                    msg = 'Try deposit funds to archived Purse Account'
                    o = Operation(purse.TYPE_BALANCE, p.id, amount, purse.STATUS_FAILED, 
                                  purse.FC_ARCHIVED_PURSE, msg)
                elif amount < 0 or amount != round(amount,2):
                    msg = 'Try deposit illegal amount (%s)' % amount
                    o = Operation(purse.TYPE_BALANCE, p.id, amount, purse.STATUS_FAILED,
                        purse.FC_ILLEGAL_AMOUNT, msg)
                else:
                    p.balance += amount
                    o = Operation(purse.TYPE_BALANCE, p.id, amount, 
                                  purse.STATUS_DONE, purse.FC_OK)
                session.add(o)
                session.commit()
                operation = o.toDict()
                return operation

            d = self.Blocks.get_lock(number)
            d.addCallback(handle, session, amount)
            d.addErrback(self.__getFailed, 'Deposit')
            return d

        log.msg('request: deposit(%s, %s)' % (number, amount))
        
        d = xquery(session, number, amount)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'deposit')
        d.addBoth(self.__fin, session, number)
        return d    
    
    def _withdraw(self, session, number, amount):
        '''
        Withdraw funds from Purse Account
        @params: number - purse account number, 
                 funds amount in account's currency
        @result: dict - result operation dict
                {id, type, accountId, status, failcode, ctime, amount, comment}
        '''
        
        def xquery(session, number, amount):
            def handle(x, session, amount):
                p = self.__getAccountByNumber(session,number)
                if not p:
                    msg = 'Try withdraw funds from not exist Purse Account'
                    log.msg(msg)
                    raise Exception(msg)
                elif p.isArchive():
                    msg = 'Try withdraw funds from archived Purse Account'
                    o = Operation(purse.TYPE_BALANCE, p.id, -amount, purse.STATUS_FAILED, 
                                  purse.FC_ARCHIVED_PURSE, msg)
                elif amount < 0 or amount != round(amount,2):
                    msg = 'Try withdraw illegal amount (%s)' % amount
                    o = Operation(purse.TYPE_BALANCE, p.id, -amount, purse.STATUS_FAILED,
                                  purse.FC_ILLEGAL_AMOUNT, msg)
                else:
                    if amount > p.blFunds:
                        msg = 'Not enough blocked funds for withdraw operation!'
                        o = Operation(purse.TYPE_BALANCE, p.id, -amount, purse.STATUS_FAILED, 
                                      purse.FC_NOT_ENOUGH_BLOCKED_FUNDS, msg)
                    else:
                        p.balance -= amount
                        p.blFunds -= amount
                        o = Operation(purse.TYPE_BALANCE, p.id, -amount, 
                                      purse.STATUS_DONE, purse.FC_OK)
                session.add(o)
                session.commit()
                operation = o.toDict()
                return operation
            
            d = self.Blocks.get_lock(number)
            d.addCallback(handle, session, amount)
            d.addErrback(self.__getFailed, 'Withdraw')
            return d

        log.msg('request: withdraw(%s, %s)' % (number, amount))

        d = xquery(session, number, amount)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'withdraw')
        d.addBoth(self.__fin, session, number)
        return d    

    def _blockFunds(self, session, number, amount):
        '''
        Block funds on Purse Account
        @params: number - purse account number, 
                 funds amount in account's currency
        @result: dict - result operation dict
                {id, type, accountId, status, failcode, ctime, amount, comment}
        '''
        def xquery(session, number, amount):
            def handle(x, session, amount):
                p = self.__getAccountByNumber(session, number)
                if not p:
                    msg = 'Try block funds on not exist Purse Account'
                    log.msg(msg)
                    raise Exception(msg)
                elif p.isArchive():
                    msg = 'Try to block funds on archived Purse Account'
                    o = Operation(purse.TYPE_BLOCK, p.id, -amount, purse.STATUS_FAILED, 
                                  purse.FC_ARCHIVED_PURSE, msg)
                elif amount < 0 or amount != round(amount,2):
                    msg = 'Try to block illegal amount (%s)' % amount
                    o = Operation(purse.TYPE_BLOCK, p.id, -amount, purse.STATUS_FAILED,
                                  purse.FC_ILLEGAL_AMOUNT, msg)
                else:
                    freeFunds = p.balance - p.blFunds
                    if amount > freeFunds:
                        msg = 'Not enough free funds for block!'
                        o = Operation(purse.TYPE_BLOCK, p.id, -amount, purse.STATUS_FAILED, 
                                      purse.FC_NOT_ENOUGH_FREE_FUNDS, msg)
                    else:
                        p.blFunds += amount
                        o = Operation(purse.TYPE_BLOCK, p.id, -amount, 
                                      purse.STATUS_DONE, purse.FC_OK)
                session.add(o)
                session.commit()
                operation = o.toDict()
                return operation
            
            d = self.Blocks.get_lock(number)
            d.addCallback(handle, session, amount)
            d.addErrback(self.__getFailed, 'blockFunds')
            return d

        log.msg('request: blockFunds(%s, %s)' % (number, amount))

        d = xquery(session, number, amount)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'blockFunds')
        d.addBoth(self.__fin, session, number)
        return d    

    def _releaseFunds(self, session, number, amount):
        '''
        release blocked funds on Purse Account
        @params: number - purse account number, 
                 funds amount in account's currency
        @result: dict - result operation dict
                {id, type, accountId, status, failcode, ctime, amount, comment}
        '''
        def xquery(session, number, amount):
            def handle(x, session, amount):
                p = self.__getAccountByNumber(session, number)
                if not p:
                    msg = 'Try release funds on not exist Purse Account'
                    log.msg(msg)
                    raise Exception(msg)
                elif p.isArchive():
                    msg = 'Try to release funds on archived Purse Account'
                    o = Operation(purse.TYPE_BLOCK, p.id, amount, purse.STATUS_FAILED, 
                                  purse.FC_ARCHIVED_PURSE, msg)
                elif amount < 0 or amount != round(amount,2):
                    msg = 'Try to release illegal amount (%s)' % amount
                    o = Operation(purse.TYPE_BLOCK, p.id, amount, purse.STATUS_FAILED,
                                  purse.FC_ILLEGAL_AMOUNT, msg)
                else:
                    if amount > p.blFunds:
                        msg = 'Not enough blocked Funds to release!'
                        o = Operation(purse.TYPE_BLOCK, p.id, amount, purse.STATUS_FAILED, 
                                      purse.FC_NOT_ENOUGH_BLOCKED_FUNDS, msg)
                    else:
                        p.blFunds -= amount
                        o = Operation(purse.TYPE_BLOCK, p.id, amount, 
                                      purse.STATUS_DONE, purse.FC_OK)
                session.add(o)
                session.commit()
                operation = o.toDict()
                return operation
            
            d = self.Blocks.get_lock(number)
            d.addCallback(handle, session, amount)
            d.addErrback(self.__getFailed, 'releaseFunds')
            return d

        log.msg('request: releaseFunds(%s, %s)' % (number, amount))

        d = xquery(session, number, amount)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'releaseFunds')
        d.addBoth(self.__fin, session, number)
        return d    
    
    def _createGroup(self, session, name):
        def xquery(session, name):
            if self.__getGroupByName(session, name):
                raise Exception('createGroup: Group already exist!')
            group = Group(name)
            session.add(group)
            session.commit()
            return group.name
        
        log.msg('request: createGroup(%s)' % (name))

        d = threads.deferToThread(xquery, session, name)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'createGroup')
        d.addBoth(self.__fin, session)
        return d    

    def _changeGroupActivity(self, session, name, activity):
        def xquery(session, name, activity):
            group = self.__getGroupByName(session, name)
            if not group:
                raise Exception('changeGroupActivity: Not exist group!')
            group.state = activity
            session.commit()
            return group.state
        
        log.msg('request: changeGroupActivity(%s, %s)' % (name, activity))

        d = threads.deferToThread(xquery, session, name, activity)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'changeGroupActivity')
        d.addBoth(self.__fin, session)
        return d    

    def _getGroups(self, session, activity=purse.ACTIVITY_ALIVE):
        '''
        getting group list for user-choice
        @result: group names array 
        '''
        def getDone(xquery):
            groups = []
            for q in xquery:
                groups.append(q.toDict())
            return self.__logData(groups)
        log.msg('request: getGroups(%s)' % activity)
        
        if activity==purse.ACTIVITY_ALL: 
            grps = session.query(Group)
        elif activity==purse.ACTIVITY_ALIVE:
            grps = session.query(Group).filter(Group.state==purse.ACTIVITY_ALIVE)
        else:
            grps = session.query(Group).filter(Group.state==purse.ACTIVITY_ARCHIV)
        d = threads.deferToThread(grps.all)
        d.addCallback(getDone)
        d.addErrback(self.__getFailed, 'getGroups')
        d.addBoth(self.__fin, session)
        return d    

    def _getPurseInfo(self, session, number):
        '''
        Get Purse account info
        @param: number - purse account number
        @result: dict - {number, currency,state, ctime, balance, freeFunds, [groups]}
                 where [groups] - dict {id, group_name} 
        '''
        def xquery(session, number):
            p = self.__getAccountByNumber(session, number)
            if not p:
                raise Exception('getPurseInfo: Purse not exist')
            res = {'number'   : p.number,
                   'currency' : p.currency,
                   'state'    : p.state,
                   'ctime'    : p.ctime,
                   'balance'  : p.balance,
                   'freeFunds': (p.balance - p.blFunds),
                   'groups'   : [g.toDict() for g in p.groups]}
            return res
        
        log.msg('request: getPurseInfo(%s)' % (number))
        
        d = threads.deferToThread(xquery, session, number)
        d.addCallback(self.__getDone)
        d.addErrback(self.__getFailed, 'getPurseInfo')
        d.addBoth(self.__fin, session)
        return d    
    
    def _getPurseList(self, session, criteria={}):
        '''
        Get Purses list info
        @param: criteria - purse account criteria query. 
                Its dict {starttime, endtime, [groups], currency},
                where starttime and endtime determine time period
                      [groups] - groups name array
                      currency - currency
        @result: array of dicts like:
                 {number, currency,state, ctime, balance, freeFunds, [groups]}
                 where [groups] - dict {id, group_name} 
        '''
        def getDone(xquery):
            accounts = []
            for q in xquery:
                accounts.append(q.toDict())
            return self.__logData(accounts)

        log.msg('request: getPurseList(%s)' % (criteria))
        query = session.query(Account)
        if criteria.get('currency', None):
            query = query.filter(Account.currency==criteria.get('currency'))
        if criteria.get('numbers', None):
            query = query.filter(Account.number.in_(criteria.get('numbers')))
        if criteria.get('startTime', None):
            query = query.filter(Account.ctime > criteria.get('startTime', None))
        if criteria.get('endTime', None):
            query = query.filter(Account.ctime < criteria.get('endTime', None))
        groups = criteria.get('groups', []) 
        if groups:
            if not (groups == []):
                query = (query.
                     join(association_table, Account.id == association_table.c.account_id).
                     join(Group, association_table.c.group_id == Group.id).
                     filter(Group.name.in_(groups)).order_by(Account.id))

        if criteria.get('offset'):
            criteria['offset'][1] = criteria.get('offset')[0] + criteria.get('offset')[1]
            d = threads.deferToThread(query.slice, criteria.get('offset', [0])[0], criteria.get('offset', [0])[1])
        else:
            d = threads.deferToThread(query.all)
        d.addCallback(getDone)
        d.addErrback(self.__getFailed, 'getPurseList')
        d.addBoth(self.__fin, session)
        return d    

    
class VaultApp(object):
    '''
    Purse application API
    '''
    def __init__(self):
        '''
        Purse application init
        '''
        configfile=self.parseCommandLineOptions()
        self.scavengerTimeOut = 120
        
        log.msg('Application init')
        self.app_init(configfile)

        log.msg('Database init')
        db.init(self.Config)
        log.msg('init done')
        
    
    def app_init(self, configfile):
        '''
        First init application procedure contain with config initializing (with 
        ConfigParser) and Site init (with Twisted Engine) 
        '''
        self.Config = ConfigParser.ConfigParser()
        self.Config.read(configfile) 
  
        self.init_site(self.Config.get('application', 'service'),
                       int(self.Config.get('application', 'port')),
                       self.Config.get('ssl', 'private-key'),
                       self.Config.get('ssl', 'ssl-certificate'))

        logdest=self.Config.get('log', 'output')
        if logdest=='stdout':
            log.startLogging(sys.stdout)
        else:
            wmode = self.Config.get('log', 'writemode')
            log.startLogging(open(logdest, wmode))

        try:
            threads = self.Config.get('application', 'threads')
        except:
            threads = None
        if threads:
            log.msg('Configuring application for %s threads' % (str(threads)))
            reactor.suggestThreadPoolSize(threads)
        reactor.callLater(self.scavengerTimeOut, self.scavenger)
        
    def init_site(self, service, port, priv_key, ssl_cert):
        '''
        Site initialize with Twisted Engine
        Service-name and port get from config file
        '''
        self._bl = BusinessLogic(self.Config)
        r = resource.Resource()
        r.putChild(service, XmlRpcFacade(self._bl))
        site = server.Site(r)
    
        sslContext = ssl.DefaultOpenSSLContextFactory(priv_key, ssl_cert)
        reactor.listenSSL(port, site, contextFactory = sslContext)
    
    def parseCommandLineOptions(self):
        '''
        command line arguments to args structure init
        '''
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', default = 'config.cfg')
        args = parser.parse_args()
        return args.config

    def scavenger(self):
        '''
        its garbage-collector for blocks
        '''
        self._bl.Blocks.clean()
        reactor.callLater(self.scavengerTimeOut, self.scavenger)
      
    def run(self):
        '''  
        Purse application run
        '''
        log.msg('Vaultd version %s runs...' % VERSION)
        reactor.run()
       

def main():
    va = VaultApp()
    va.run() 

if __name__ == '__main__':
    main()