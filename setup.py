#!/usr/bin/env python
from distutils.core import setup
dist = setup(
    name='vaultd',
    version='0.2',
    description = 'Purse storage service',
    long_description = 'This service allows to allocate funds with Purse',
    author='Aleksandr Stepkin',
    author_email='aleksandr.stepkin@brocompany.com',
    url='ssh://git@dev.brocompany.com:2200/money-vault.git',
    license='Broco',

    package_dir={'':'src'},
    packages=['vaultd'],

    data_files=[('/usr/bin',['install/moneyvaultd'])]
)
